
import requests
import os
from django.conf import settings

def get_lat_lng(street, city, zip, country):

    if not street and not city and not zip and not country:
        return None, None

    base_url = "https://nominatim.openstreetmap.org/search?"
    params = {
        "format": "json",
        "q": f"{street} {city} {zip} {country}"
    }
    try:
        response = requests.get(base_url, params=params)
    except requests.exceptions.ConnectionError:
        print(f"could not determine location of address! Connection error.")
        return None, None

    if response.status_code != 200:
        print(f"could not determine location of address! url: {response.url} with: {response}")
        return None, None

    data = response.json()

    try:
        if data[0]['lat'] and data[0]['lon']:
            return data[0]['lat'], data[0]['lon']
    except: pass # todo: handle error and forward it to frontend
    return None, None

def deleteFile(path):
    if os.path.exists(path) and os.path.isfile(path):
        os.remove(path)
    else:
        print(f"The file {path} does not exist")
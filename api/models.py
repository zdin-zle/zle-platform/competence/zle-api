from api.utils import utils
from django.db import models
from django.conf import settings

# todo: get rid of position attribute (bc its useless, a user can not have multiple positions in the same profile)
#   -> position info gets saved by putting the user in different arrays in the profile
class User(models.Model):
    firstname   = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH)
    lastname    = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH)
    title       = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH, blank=True, null=True)
    email       = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH)
    position    = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH)
    photo       = models.CharField(max_length = settings.FILE_FIELDS_LENGTH)
    orc_id      = models.CharField(max_length = 20, blank=True, null=True)
    # photo       = models.ImageField(upload_to='user/%Y/%m/%d/', blank=True, null=True)
    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.email

class ResearchProject(models.Model):
    name        = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH)
    description = models.CharField(max_length = settings.TEXT_AREA_FORM_FIELDS_LENGTH, blank=True, null=True)
    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.name
    
class ResearchLab(models.Model):
    name        = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH)
    description = models.CharField(max_length = settings.TEXT_AREA_FORM_FIELDS_LENGTH, blank=True, null=True)
    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.name

class ResearchCluster(models.Model):
    name        = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH)
    description = models.CharField(max_length = settings.TEXT_AREA_FORM_FIELDS_LENGTH)
    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.name

class Address(models.Model):
    state       = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH)
    street      = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH)
    zip         = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH)
    city        = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH)
    country     = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH)
    latitude    = models.FloatField(blank=True, null=True)
    longitude   = models.FloatField(blank=True, null=True)

    # override save method to calculate lat and lng from address if new address is created
    def save(self, *args, **kwargs):
        if not self.latitude or not self.longitude:
            lat, lng = utils.get_lat_lng(self.street, self.city, self.zip, self.country)
            self.latitude = lat
            self.longitude = lng
        super().save(*args, **kwargs)

    class Meta:
        unique_together = ('street', 'zip', 'city', 'country')
        ordering = ['-id']

class ResearchNetworkEvent(models.Model):
    name        = models.CharField(max_length = settings.EVENTS_NEWS_FORM_FIELDS_LENGTH)
    description = models.CharField(max_length = settings.TEXT_AREA_FORM_FIELDS_LENGTH, blank=True, null=True)
    photo       = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH, blank=True, null=True) # models.ImageField(upload_to='user/%Y/%m/%d/', blank=True, null=True) TODO replace
    date        = models.DateTimeField(auto_now_add=False)
    address     = models.ForeignKey(Address, on_delete=models.SET_NULL, blank=True, null=True)
    created     = models.DateTimeField(auto_now_add=True)
    updated     = models.DateTimeField(auto_now=True)
    class Meta:
        ordering = ['-date']
    def __str__(self):
        return self.name

class ResearchNetworkNews(models.Model):
    title       = models.CharField(max_length = settings.EVENTS_NEWS_FORM_FIELDS_LENGTH)
    photo       = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH, blank=True, null=True) # models.ImageField(upload_to='user/%Y/%m/%d/', blank=True, null=True) TODO replace
    created     = models.DateTimeField(auto_now_add=True)
    updated     = models.DateTimeField(auto_now=True)
    description = models.CharField(max_length = settings.TEXT_AREA_FORM_FIELDS_LENGTH)
    eyecatcher  = models.CharField(max_length = settings.EVENTS_NEWS_FORM_FIELDS_LENGTH, blank=True, null=True)
    class Meta:
        ordering = ['-created']
    def __str__(self):
        return self.title

class ResearchNetwork(models.Model):
    shortcut                = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH)
    name                    = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH)
    logo                    = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH)
    events                  = models.ManyToManyField(ResearchNetworkEvent, blank=True)
    description             = models.CharField(max_length = settings.TEXT_AREA_FORM_FIELDS_LENGTH, blank=True, null=True)
    research_description    = models.CharField(max_length = settings.TEXT_AREA_FORM_FIELDS_LENGTH, blank=True, null=True)
    news                    = models.ManyToManyField(ResearchNetworkNews, blank=True)
    address                 = models.ForeignKey(Address, on_delete=models.SET_NULL, blank=True, null=True)
    external_link           = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH, blank=True, null=True)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.name

class Institute(models.Model):
    title               = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH)
    shortcut            = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH)
    description         = models.CharField(max_length = settings.TEXT_AREA_FORM_FIELDS_LENGTH, blank=True, null=True)
    # address             = models.ForeignKey(Address, on_delete=models.SET_NULL, blank=True, null=True)
    external_link       = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH, blank=True, null=True)
    logo                = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH)
    class Meta:
        ordering = ['-id']

    # def __str__(self):
    #     return self.title
    
class CompetenceProfile(models.Model):
    created             = models.DateTimeField(auto_now_add=True)
    updated             = models.DateTimeField(auto_now=True)

    title               = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH)
    organization        = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH, blank=True, null=True)
    shortcut            = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH)
    logo                = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH)
    description         = models.CharField(max_length = settings.TEXT_AREA_FORM_FIELDS_LENGTH)
    research_focus      = models.CharField(max_length = settings.TEXT_AREA_FORM_FIELDS_LENGTH, blank=True, null=True)
    external_link       = models.CharField(max_length = settings.TEXT_FORM_FIELDS_LENGTH, blank=True, null=True)
    publications        = models.CharField(max_length = settings.FILE_FIELDS_LENGTH, blank=True, null=True)

    author              = models.ForeignKey(User, related_name='competence_profile', on_delete=models.PROTECT, blank=True, null=True)
    address             = models.ForeignKey(Address,   on_delete=models.SET_NULL, blank=True, null=True)
    institute           = models.ForeignKey(Institute, on_delete=models.SET_NULL, blank=True, null=True)

    representatives     = models.ManyToManyField(User, blank=True)
    maintainers         = models.ManyToManyField(User, blank=True)   
    
    representatives = models.ManyToManyField(
        User,
        related_name='representatives',
        blank=True
    )
    maintainers = models.ManyToManyField(
        User,
        related_name='maintainers',
        blank=True
    ) 

    research_projects   = models.ManyToManyField(ResearchProject, blank=True)
    research_labs       = models.ManyToManyField(ResearchLab,     blank=True)
    research_networks   = models.ManyToManyField(ResearchNetwork, blank=True)
    research_clusters   = models.ManyToManyField(ResearchCluster, blank=True)
     
    # publications_file   = models.FileField(upload_to='bibliography', blank=True, null=True) # not used currently
    class Meta:
        ordering = ['-created']
from django.contrib import admin
from django.urls import path

from api.models import Institute
from . import views
from django.conf import settings
from django.conf.urls.static import static
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView

from api.views import BibfileDetailView, ClusterMapListView, InstituteDetailView, InstituteListView, NetworkMapListView, PostClusterView, PostEventView, PostInstituteView, PostLabView, PostNetworkView, PostNewsView, PostProfileView, PostProjectView, ProfileListView, NetworkListView, ClusterListView, ProfileMapListView, ProjectListView, LabListView, ProfileDetailView, NetworkDetailView, ClusterDetailView

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

# urls can be viewed an tested out with swagger
# if served locally, view it on:
# http://localhost:8000/competencebackend/swagger/
# or
# https://dev-zle.offis.de/competencebackend/swagger/
schema_view = get_schema_view(
    openapi.Info(
        title="Competence API",
        default_version='v1',
        description="API for the Competence Backend of the ZLE project",
        contact=openapi.Contact(email="c.bettermann@tu-bs.de"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    ### list views ###
    # input:
    # - search parameter 
    #   - the search parameter can search in multiple fields
    #   - these fields are predefined in the specific views
    # output:
    # - list of objects in not so detailed format that match the search value and filter parameters
    path('competencebackend/institutes', InstituteListView.as_view(), name='institute-list'),
    path('competencebackend/profiles',   ProfileListView.as_view(),   name='profile-list'),
    path('competencebackend/networks',   NetworkListView.as_view(),   name='network-list'),
    path('competencebackend/clusters',   ClusterListView.as_view(),   name='cluster-list'),
    path('competencebackend/projects',   ProjectListView.as_view(),   name='project-list'),
    path('competencebackend/labs',       LabListView.as_view(),       name='lab-list'),

    ### detail views ###
    # input:
    # - id of the object
    # output:
    # - everything about the object including its relations
    path('competencebackend/institute/<int:id>',       InstituteDetailView.as_view(), name='institute-detail'),
    path('competencebackend/profile/<int:id>',         ProfileDetailView.as_view(),   name='profile-detail'),
    path('competencebackend/network/<int:id>',         NetworkDetailView.as_view(),   name='network-detail'),
    path('competencebackend/cluster/<int:id>',         ClusterDetailView.as_view(),   name='cluster-detail'),
    path('competencebackend/bibfile/<str:filename>',   BibfileDetailView.as_view(),   name='bibfile-detail'),

    ### views for map ###
    # function the same as the list views
    # input:
    # - search parameter
    # output:
    # - list of objects in a format that can be used for the map
    path('competencebackend/profiles_map', ProfileMapListView.as_view(), name='profile-map'),
    path('competencebackend/networks_map', NetworkMapListView.as_view(), name='network-map'),
    path('competencebackend/clusters_map', ClusterMapListView.as_view(), name='cluster-map'),    

    ### post and update views ###
    # input:
    # - json object with the data
    #   - json obect can hold an id field -> then the object with the id is updated
    #   - if no id is given, or the id cant be found, a new object is created
    # output:
    # - the created or updated object 
    path('competencebackend/post_institute/', PostInstituteView.as_view(), name='post_institute'),
    path('competencebackend/post_profile/',   PostProfileView.as_view(),   name='post_profile'),
    path('competencebackend/post_network/',   PostNetworkView.as_view(),   name='post_network'),
    path('competencebackend/post_cluster/',   PostClusterView.as_view(),   name='post_cluster'),
    path('competencebackend/post_project/',   PostProjectView.as_view(),   name='post_project'),
    path('competencebackend/post_lab/',       PostLabView.as_view(),       name='post_lab'),
    path('competencebackend/post_event/',     PostEventView.as_view(),     name='post_event'),
    path('competencebackend/post_news/',      PostNewsView.as_view(),      name='post_news'),
    # path('competencebackend/post_project/', views.post_project),

    ### upload views ###
    # input:
    # - file
    # - allowedTypes
    # output:
    # - the url to the file
    path('competencebackend/upload_image/', views.upload_file, kwargs={'allowedTypes': ['jpg', 'jpeg', 'png', 'gif', 'svg', 'webp']} ),
    path('competencebackend/upload_bib/',   views.upload_file, kwargs={'allowedTypes': ['bib']}                                      ),

    ### delete views ###
    # input:
    # - id of the object
    path('competencebackend/delete_institute/<int:id>/',    views.delete_institute              ),
    path('competencebackend/delete_profile/<int:id>/',      views.delete_profile                ),
    path('competencebackend/delete_network/<int:id>/',      views.delete_network                ),
    path('competencebackend/delete_cluster/<int:id>/',      views.delete_cluster                ),
    path('competencebackend/delete_event/<int:id>/',        views.delete_event                  ),
    path('competencebackend/delete_news/<int:id>/',         views.delete_news                   ),
    path('competencebackend/delete_project/<int:id>/',      views.delete_project                ),
    path('competencebackend/delete_lab/<int:id>/',          views.delete_lab                    ),
    path('competencebackend/delete_project_from_profile/',  views.delete_project_from_profile   ),
    path('competencebackend/delete_lab_from_profile/',      views.delete_lab_from_profile       ),
    # path('competencebackend/delete_user/<int:id>/', views.delete_user),

    ### debug urls ###
    path('competencebackend/admin/',   admin.site.urls),
    path('competencebackend/schema/',  SpectacularAPIView.as_view(),                       name='schema'  ),
    path('competencebackend/swagger/', SpectacularSwaggerView.as_view(url_name='schema'),  name='swagger' ),
    
]

schema_view = SpectacularAPIView.as_view()

admin.site.site_header = 'ZLE Administration page'
admin.site.site_title  = 'ZLE'
admin.site.index_title = 'Admin'

if settings.DEBUG:
  urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

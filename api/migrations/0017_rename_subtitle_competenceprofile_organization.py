# Generated by Django 4.2.6 on 2023-12-15 10:54

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0016_delete_logo'),
    ]

    operations = [
        migrations.RenameField(
            model_name='competenceprofile',
            old_name='subtitle',
            new_name='organization',
        ),
    ]

# Generated by Django 4.2.6 on 2023-12-15 11:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0017_rename_subtitle_competenceprofile_organization'),
    ]

    operations = [
        migrations.AddField(
            model_name='competenceprofile',
            name='external_link',
            field=models.CharField(blank=True, max_length=1024),
        ),
        migrations.AddField(
            model_name='researchnetwork',
            name='external_link',
            field=models.CharField(blank=True, max_length=1024),
        ),
    ]

# Generated by Django 5.0.2 on 2024-06-16 17:10

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0029_rename_street1_address_street'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='competenceprofile',
            name='workgroup',
        ),
        migrations.AlterUniqueTogether(
            name='address',
            unique_together={('street', 'zip', 'city', 'country')},
        ),
    ]

from datetime import datetime
import os
import random
import string

from django.db import transaction
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.generics import ListAPIView, GenericAPIView
from rest_framework.views import APIView
from rest_framework.exceptions import ValidationError
from drf_spectacular.utils import extend_schema

from api.models import Address, CompetenceProfile, Institute, ResearchLab, ResearchNetwork, ResearchCluster, ResearchNetworkEvent, ResearchNetworkNews, ResearchProject, ResearchLab, User
from api.serializers import AddressInputSerializer, ClusterInputSerializer, ClusterMapSerializer, InstituteInputSerializer, InstituteSerializer, NetworkEventInputSerializer, NetworkEventSerializer, NetworkInputSerializer, NetworkListSerializer, NetworkMapSerializer, NetworkNewsInputSerializer, NetworkNewsSerializer, ProfileInputSerializer, ProfileMapSerializer, ProfileDetailSerializer, NetworkDetailSerializer, ClusterListSerializer, ProjectSerializer, LabSerializer, ProfileListSerializer, ClusterDetailSerializer, QuerySerializer, UploadFileSerializer
import api.utils.bibjson as bibjson
from api.utils import utils


### Base Classes ###
class BaseListView(ListAPIView):
  """
  Base class for list views of the items, when they get displayed in lists. They only contain the necessary information for displaying in the list.
  Searches in all of the given search_fields and returns the entries that contain the search term
  """

  model = None
  serializer_class = None
  search_fields = []

  # used to retrieve the search term from the request query parameters
  # using the `QuerySerializer`. It ensures that the search term is valid and returns it.
  def get_search_term(self):
    serializer = QuerySerializer(data=self.request.query_params)
    serializer.is_valid(raise_exception=True)
    return serializer.validated_data.get('search', '')

  # used to retrieve the queryset for the view. It fetches the search term,
  # filters the queryset based on the search term, and returns the filtered queryset.
  def get_queryset(self):
    search_term = self.get_search_term()
    queryset = self.model.objects.all()
    if search_term:
      filters = [f'{field}__icontains' for field in self.search_fields]
      queryset = queryset.filter(**{f: search_term for f in filters})
    return queryset

  # overwrites parent method
  def list(self, request, *args, **kwargs):
    queryset = self.get_queryset()
    serializer = self.get_serializer_class()(queryset, many=True)
    return Response(serializer.data)

class BaseDetailView(APIView):
  """
  Base class for detailed views.
  It is used to retrieve a single item by its ID and return all its corresponding information. Including all relationships.
  """
  model = None
  serializer_class = None

  # specifies get method, to signalize that this is a get request
  def get(self, request, id):
    obj = get_object_or_404(self.model, id=id)
    serializer = self.serializer_class(obj)
    return Response(serializer.data, status=status.HTTP_200_OK)

### List views (implement BaseListView) ###
class InstituteListView(BaseListView):
  """
  Lists all entries, or filters them by a search term if it is provided.
  Search Parameter is for whatever reason not displayed in swagger. It is simply called "search".\n
  .../competencebackend/institutes?search=foo
  """

  # override get queryset method to filter the profiles of the institute
  def get_queryset(self):
    search_term = self.get_search_term()
    # queryset = self.model.objects.all()
    ProfilesQueryset = CompetenceProfile.objects.all()
    InstituteQueryset = Institute.objects.all()

    if search_term:
      filters = [f'{field}__icontains' for field in self.search_fields]
      ProfilesQueryset = ProfilesQueryset.filter(**{f: search_term for f in filters})
      # get all institutes of these filtered profiles
      instituteIDs = ProfilesQueryset.values('institute').distinct()
      InstituteQueryset = Institute.objects.filter(id__in=[item['institute'] for item in instituteIDs])

    return InstituteQueryset

  model = Institute
  serializer_class = InstituteSerializer
  search_fields = ['name', 'shortcut']

class ProfileListView(BaseListView):
  """
  Lists all entries, or filters them by a search term if it is provided.
  Search Parameter is for whatever reason not displayed in swagger. It is simply called "search".\n
  .../competencebackend/profiles?search=foo
  """
  model = CompetenceProfile
  serializer_class = ProfileListSerializer
  search_fields = ['name', 'shortcut']

class NetworkListView(BaseListView):
  """
  Lists all entries, or filters them by a search term if it is provided.
  Search Parameter is for whatever reason not displayed in swagger. It is simply called "search".\n
  .../competencebackend/networks?search=foo
  """
  model = ResearchNetwork
  serializer_class = NetworkListSerializer
  search_fields = ['name', 'shortcut']

class ClusterListView(BaseListView):
  """
  Lists all entries, or filters them by a search term if it is provided.
  Search Parameter is for whatever reason not displayed in swagger. It is simply called "search".\n
  .../competencebackend/clusters?search=foo
  """
  model = ResearchCluster
  serializer_class = ClusterListSerializer
  search_fields = ['name']
  
class ProjectListView(BaseListView):
  """
  Lists all entries, or filters them by a search term if it is provided.
  Search Parameter is for whatever reason not displayed in swagger. It is simply called "search".\n
  .../competencebackend/projects?search=foo
  """
  model = ResearchProject
  serializer_class = ProjectSerializer
  search_fields = ['name']

class LabListView(BaseListView):
  """
  Lists all entries, or filters them by a search term if it is provided.
  Search Parameter is for whatever reason not displayed in swagger. It is simply called "search".\n
  .../competencebackend/labs?search=foo
  """
  model = ResearchLab
  serializer_class = LabSerializer
  search_fields = ['name']

### Detail views (implement BaseDetailView) ###
class InstituteDetailView(BaseDetailView):
  model = Institute
  serializer_class = InstituteSerializer

class ProfileDetailView(BaseDetailView):
  model = CompetenceProfile
  serializer_class = ProfileDetailSerializer

class NetworkDetailView(BaseDetailView):
  model = ResearchNetwork
  serializer_class = NetworkDetailSerializer

class ClusterDetailView(BaseDetailView):
  model = ResearchCluster
  serializer_class = ClusterDetailSerializer

class BibfileDetailView(GenericAPIView):
  """
  Returns the content of a bibtex file directly as json.
  """

  def get(self, request, filename):    
    bibtex_file = 'media/publications/' + filename
    # without file extension
    collection = os.path.splitext(os.path.basename(bibtex_file))[0]
    try:
      with open(bibtex_file) as f:
        bibtex_str = f.read()
      bibjson_collection = bibjson.collection_from_bibtex_str(bibtex_str,
                                                              collection=collection,
                                                              source=bibtex_file,
                                                              created=datetime.utcnow().isoformat())
      return Response(bibjson_collection, status=status.HTTP_200_OK)

    except FileNotFoundError:
      return Response([], status=status.HTTP_200_OK)

    except Exception as e:
      return Response({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

### Map views ###
class ProfileMapListView(BaseListView):
  """
  Lists all entries, or filters them by a search term if it is provided.
  Search Parameter is for whatever reason not displayed in swagger. It is simply called "search".\n
  .../competencebackend/profiles_map?search=foo
  """
  model = CompetenceProfile
  serializer_class = ProfileMapSerializer
  search_fields = ['name', 'shortcut']

class NetworkMapListView(BaseListView):
  """
  Lists all entries, or filters them by a search term if it is provided.
  Search Parameter is for whatever reason not displayed in swagger. It is simply called "search".\n
  .../competencebackend/networks_map?search=foo
  """
  model = ResearchNetwork
  serializer_class = NetworkMapSerializer
  search_fields = ['name', 'shortcut']

class ClusterMapListView(BaseListView):
  """
  Lists all entries, or filters them by a search term if it is provided.
  Search Parameter is for whatever reason not displayed in swagger. It is simply called "search".\n
  .../competencebackend/clusters_map?search=foo
  """
  model = ResearchCluster
  serializer_class = ClusterMapSerializer
  search_fields = ['name']


# Helper Functions
def fetchOrCreateUser(userDict):
  """
  it looks in the database if the user already exists.
  it does that by checking the id or email of the person.
  if not it creates a new user.
  """
  def updateUserFields(user):
    user.firstname = userDict.get('firstname', '')
    user.lastname = userDict.get('lastname', '')
    user.title = userDict.get('title', '')
    user.position = userDict.get('position', '')
    user.photo = userDict.get('photo', '')
    user.email = userDict.get('email', '')
    user.orc_id = userDict.get('orc_id', '')
    user.save()
    return user

  # Try to fetch the user by ID
  if userDict.get('id'):
    try:
      user = User.objects.get(id=userDict['id'])
      return updateUserFields(user)
    except User.DoesNotExist:
      pass
  # Try to fetch the user by email
  try:
    user = User.objects.get(email=userDict['email'])
  except User.DoesNotExist:
    user = User()
  return updateUserFields(user)

def fetchOrCreateAddress(addressDict):
  # try to fetch address by its values
  try:
    address = Address.objects.filter(
      street__iexact  = addressDict['street'].strip(),
      zip__iexact     = addressDict['zip'].strip(),
      city__iexact    = addressDict['city'].strip(),
      country__iexact = addressDict['country'].strip()
    )
    address = address[0]
  except (Address.DoesNotExist, IndexError):
    # if not found validate the input then
    input_serializer = AddressInputSerializer(data=addressDict)
    input_serializer.is_valid(raise_exception=True)

    # then create new address
    address = Address(
      street  = addressDict['street'],
      zip     = addressDict['zip'],
      city    = addressDict['city'],
      country = addressDict['country'],
    )
  address.save()
  return address

### Post views ###
class PostInstituteView(APIView):
  # create new or update existing institute
  @extend_schema(
    request=InstituteInputSerializer,
    responses=InstituteInputSerializer,
    description="Create or update an institute. ID is optional. If provided, the institute with the given ID will be updated."
  )
  @transaction.atomic
  def post(self, request):
    body = request.data
    id = body.get('id')

    if id:
      try:
        institute = Institute.objects.get(id=id)
      except Institute.DoesNotExist:
        return Response({"error": "Institute not found"}, status=status.HTTP_404_NOT_FOUND)
    else:
      institute = Institute()

    serializer = InstituteInputSerializer(institute, data=body)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
  
class PostProfileView(APIView):
  @extend_schema(
    request=ProfileInputSerializer,
    responses=ProfileDetailSerializer,
    description="""
      Creates a profile. Or tries to updated it, if ID is provided in the json.
      - it fetches existing or creates new address and representatives/maintainers.
      - it fetches existing or creates new competence profile.
        - It updates/creates all fields and the relationships of a competence profile.
    """
  )
  @transaction.atomic
  def post(self, request):
    "overrides main post method"
    body = request.data

    # fetch or create address
    address = fetchOrCreateAddress(body.get("address"))
    if isinstance(address, Response):
      return address
    body["address"] = address.id

    # validate remaining input
    input_serializer = ProfileInputSerializer(data=body)
    input_serializer.is_valid(raise_exception=True)

    # fetch and update or create profile
    competence_profile, created = CompetenceProfile.objects.get_or_create(id=body.get('id'))
    self.update_competence_profile(competence_profile, body, address)

    # creates output
    serializer = ProfileDetailSerializer(competence_profile)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

  def update_competence_profile(self, competence_profile, body, address):
    "update all fields and its corresponding relationships of a competence profile."

    competence_profile.title = body["title"]
    competence_profile.shortcut = body["shortcut"]
    competence_profile.organization = body["organization"]
    competence_profile.institute = Institute.objects.get(id=body.get('institute'))
    competence_profile.logo = body["logo"]
    competence_profile.description = body["description"]
    competence_profile.research_focus = body["research_focus"]
    competence_profile.publications = body["publications_file"]
    competence_profile.address = address
    competence_profile.external_link = body["external_link"]
    competence_profile.save()

    self.set_relationships(competence_profile, body)
    self.update_users(competence_profile, body)

  def set_relationships(self, competence_profile, body):
    relationships = {
      "research_projects": ResearchProject,
      "research_labs":     ResearchLab,
      "research_networks": ResearchNetwork,
      "research_clusters": ResearchCluster,
    }

    for key, model in relationships.items():
      stripped_key = key.removeprefix("research_")

      items = model.objects.filter(id__in=body.get(stripped_key, []))
      getattr(competence_profile, key).set(items)

  def update_users(self, competence_profile, body):
    self.clear_and_add_users(competence_profile, 'representatives', body)
    self.clear_and_add_users(competence_profile, 'maintainers', body)

  def clear_and_add_users(self, competence_profile, user_type, body):
    """
    clear users first 
    then add the new ones
    """
    getattr(competence_profile, user_type).clear()
    user_models = [fetchOrCreateUser(user) for user in body.get(user_type, [])]
    if user_models:
      getattr(competence_profile, user_type).add(*user_models)

class PostProjectView(APIView):
  @extend_schema(
    request = ProjectSerializer,
    responses = ProjectSerializer,
    description="Create or update a project. ID is optional. If provided, the project with the given ID will be updated."
  )
  @transaction.atomic
  def post(self, request):
    id = request.data.get('id')

    if id: 
      try:
        project = ResearchProject.objects.get(id=id)
      except ResearchProject.DoesNotExist:
        return Response({"error": "Project not found"}, status=status.HTTP_404_NOT_FOUND)
    else:
      project = ResearchProject()

    serializer = ProjectSerializer(project, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class PostLabView(APIView):
  @extend_schema(
    request = LabSerializer,
    responses = LabSerializer,
    description="Create or update a lab. ID is optional. If provided, the lab with the given ID will be updated."
  )
  @transaction.atomic
  def post(self, request):
    id = request.data.get('id')

    if id:
      try:
        lab = ResearchLab.objects.get(id=id)
      except ResearchLab.DoesNotExist:
        return Response({"error": "Lab not found"}, status=status.HTTP_404_NOT_FOUND)
    else:
      lab = ResearchLab()

    serializer = LabSerializer(lab, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
  
class PostClusterView(APIView):
  @extend_schema(
    request = ClusterInputSerializer,
    responses = ClusterListSerializer,
    description="Create a new research cluster. ID is optional. If provided, the cluster with the given ID will be updated."
  )
  @transaction.atomic
  def post(self, request):
    body = request.data
    inputSerializer = ClusterInputSerializer(data=body)

    if inputSerializer.is_valid():
      cluster, created = ResearchCluster.objects.get_or_create(id=body.get('id'))

      # If the cluster existed previously, remove it from all competence profiles
      if not created:
        for profile in CompetenceProfile.objects.all():
          profile.research_clusters.remove(cluster)

      cluster.name = body["name"]
      cluster.description = body["description"]
      cluster.save()

      # add the cluster back to the speicified profile list
      if body.get("profiles"):
        for profile in body["profiles"]:
          CompetenceProfile.objects.get(id=profile).research_clusters.add(cluster)

      serializer = ClusterListSerializer(cluster)
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(inputSerializer.errors, status=status.HTTP_400_BAD_REQUEST)

class PostNetworkView(APIView):
  @extend_schema(
    request = NetworkInputSerializer,
    responses = NetworkDetailSerializer,
    description="Create or update a new research network. ID is optional. If provided, the network with the given ID will be updated."
  )
  @transaction.atomic
  def post(self, request):
    body = request.data

    address = fetchOrCreateAddress(body.get("address"))
    if type(address) == Response:
      return address
    body["address"] = address.id

    input_serializer = NetworkInputSerializer(data=body)
    input_serializer.is_valid(raise_exception=True)
  
    network, created = ResearchNetwork.objects.get_or_create(id=body.get('id'))
    if not created:
      for profile in CompetenceProfile.objects.all():
        profile.research_networks.remove(network)

    network.name = body["name"]
    network.shortcut = body["shortcut"]
    network.logo = body["logo"]
    network.description = body["description"]
    network.research_description = body["research_description"]
    network.address = address
    network.external_link = body.get("external_link")
    network.save()

    if body.get("profiles"):
      for profile in body["profiles"]:
        CompetenceProfile.objects.get(id=profile).research_networks.add(network)

    serializer = NetworkDetailSerializer(network)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

class PostEventView(APIView):
  @extend_schema(
    request = NetworkEventInputSerializer,
    responses = NetworkEventSerializer,
    description="Create or update an event. ID is optional. If provided, the event with the given ID will be updated."
  )
  @transaction.atomic
  def post(self, request):
    body = request.data

    address = fetchOrCreateAddress(body.get("address"))
    if type(address) == Response:
      return address
    body["address"] = address.id

    input_serializer = NetworkEventInputSerializer(data=body)
    input_serializer.is_valid(raise_exception=True)

    try:
      network = ResearchNetwork.objects.get(id=body["networkID"])
    except ResearchNetwork.DoesNotExist:
      return Response(f'Network with ID {body["networkID"]} does not exist.', status=status.HTTP_404_NOT_FOUND)
    
    if body.get('id'):
      try:
        event = ResearchNetworkEvent.objects.get(id=body['id'])
      except ResearchNetworkEvent.DoesNotExist:
        return Response(f'Event with ID {body["id"]} does not exist.', status=status.HTTP_404_NOT_FOUND)
    else:
      event = ResearchNetworkEvent(
        name = body["name"],
        description = body["description"],
        photo = body["photo"],
        date = body["date"],
        address = address
      )

    event.save()
    network.events.add(event)
    serializer = NetworkEventSerializer(event)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

class PostNewsView(APIView):
  @extend_schema(
    request = NetworkNewsInputSerializer,
    responses = NetworkNewsSerializer,
    description="Create or update a news. ID is optional. If provided, the news with the given ID will be updated."
  )
  @transaction.atomic
  def post(self, request):
    body = request.data

    input_serializer = NetworkNewsInputSerializer(data=body)
    input_serializer.is_valid(raise_exception=True)
  
    try:
      network = ResearchNetwork.objects.get(id=body["networkID"])
    except ResearchNetwork.DoesNotExist:
      return Response(f'Network with ID {body["networkID"]} does not exist.', status=status.HTTP_404_NOT_FOUND)
    
    if body.get('id'):
      try:
        news = ResearchNetworkNews.objects.get(id=body['id'])
      except ResearchNetworkNews.DoesNotExist:
        return Response(f'News with ID {body["id"]} does not exist.', status=status.HTTP_404_NOT_FOUND)
    else:
      news = ResearchNetworkNews(
        title = body["title"],
        description = body["description"],
        photo = body["photo"],
        eyecatcher = body.get("eyecatcher")
      )
    news.save()
    network.news.add(news)
    serializer = NetworkNewsSerializer(news)
    return Response(serializer.data, status=status.HTTP_201_CREATED)


@extend_schema(
  request = UploadFileSerializer,
  responses = {"200": {'description': "filename: filename"}, "400": {'description': "Filetype not supported"}}
)
@transaction.atomic
@api_view(['POST'])
def upload_file(request, allowedTypes):
  """
  Saves a file to the media folder wit a random file name. The file is saved in the folder specified by the fileclass parameter.
  TODO: rewrite this completely and use file fields in the models -> security risk if deployed publicly

  Args:
    request (Request): request object 
      - containing the file object 
      - and the fileclass as String ('events', 'logos', 'news', 'publications', 'representatives') -> determines folder name in which the file is saved in the media folder
    allowedTypes (list): list of accepted filetypes

  Returns:
    Response: filename of the uploaded file
  """
  randomFileName = ''

  for fileclass, file in request.FILES.items():
    # fileclass -> name given in frontend
    # file.name -> original name

    fileExtension = file.name.split('.')[-1].lower()
    if fileExtension not in allowedTypes:
      return Response(status=status.HTTP_400_BAD_REQUEST, data={"error": "Filetype not supported"})
    
    while True:
      #specify RANDOM string
      randomFileName = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(10)) + '.' + fileExtension

      # read all files in folder and check if string already taken:
      if randomFileName in os.listdir('media/' + fileclass): continue
      break

    with open('media/' + fileclass + '/' + randomFileName, 'wb+') as destination:
      for chunk in file.chunks():
        destination.write(chunk)

  if not randomFileName: return Response(status=status.HTTP_400_BAD_REQUEST, data={"error": "No file uploaded"})
  return Response(status=status.HTTP_200_OK, data={"filename": randomFileName})


#delete Stuff
#todo: refactor into classes
#todo: check if user is still associated with other profiles, when deleting a profile and so its corresponding users

@transaction.atomic
@api_view(['DELETE'])
def delete_institute(request, id):
  try:
    institute = Institute.objects.get(id=id)
  except (Institute.DoesNotExist, ValueError):
    return Response(f'Deletion failed, Institute with ID {id} does not exist.', status=status.HTTP_404_NOT_FOUND)
  institute.delete()
  return Response(status=status.HTTP_200_OK)

@transaction.atomic
@api_view(['DELETE'])
def delete_profile(request, id):
  try:
    competence_profile = CompetenceProfile.objects.get(id=id)
  except (CompetenceProfile.DoesNotExist, ValueError):
    return Response(f'Deletion failed, CompetenceProfile with ID {id} does not exist.', status=status.HTTP_404_NOT_FOUND)

  utils.deleteFile('media/logos/' + competence_profile.logo)
  utils.deleteFile('media/publications/' + competence_profile.publications)
  # deprecated
  # competence_profile.publications_file.delete()


  for user in competence_profile.representatives.all():
    utils.deleteFile('media/representatives/' + user.photo)
    user.delete()

  for user in competence_profile.maintainers.all():
    utils.deleteFile('media/representatives/' + user.photo)
    user.delete()

  competence_profile.delete()
  return Response(status=status.HTTP_200_OK)

@transaction.atomic
@api_view(['DELETE'])
def delete_network(request, id):
  try:
    network = ResearchNetwork.objects.get(id=id)
  except (ResearchNetwork.DoesNotExist, ValueError):
    return Response(f'Deletion failed, Research Network with ID {id} does not exist.', status=status.HTTP_404_NOT_FOUND)

  utils.deleteFile('media/logos/' + network.logo)
  network.delete()

  return Response(status=status.HTTP_200_OK)

@transaction.atomic
@api_view(['DELETE'])
def delete_cluster(request, id):
  try:
    cluster = ResearchCluster.objects.get(id=id)
  except (ResearchCluster.DoesNotExist, ValueError):
    return Response(f'Deletion failed, Cluster with ID {id} does not exist.', status=status.HTTP_404_NOT_FOUND)

  cluster.delete()
  return Response(status=status.HTTP_200_OK)

@transaction.atomic
@api_view(['DELETE'])
def delete_event(request, id):
  try:
    event = ResearchNetworkEvent.objects.get(id=id)
  except (ResearchNetworkEvent.DoesNotExist, ValueError):
    return Response(f'Deletion failed, Event with ID {id} does not exist.', status=status.HTTP_404_NOT_FOUND)

  utils.deleteFile('media/events/' + event.photo)
  event.delete()
  return Response(status=status.HTTP_200_OK)

@transaction.atomic
@api_view(['DELETE'])
def delete_news(request, id):
  try:
    news = ResearchNetworkNews.objects.get(id=id)
  except (ResearchNetworkNews.DoesNotExist, ValueError):
    return Response(f'Deletion failed, News with ID {id} does not exist.', status=status.HTTP_404_NOT_FOUND)

  utils.deleteFile('media/news/' + news.photo)
  news.delete()
  return Response(status=status.HTTP_200_OK)

@transaction.atomic
@api_view(['DELETE'])
def delete_project(request, id):
  try:
    project = ResearchProject.objects.get(id=id)
  except (ResearchProject.DoesNotExist, ValueError):
    return Response(f'Deletion failed, Project with ID {id} does not exist.', status=status.HTTP_404_NOT_FOUND)

  project.delete()
  return Response(status=status.HTTP_200_OK)

@transaction.atomic
@api_view(['DELETE'])
def delete_lab(request, id):
  try:
    lab = ResearchLab.objects.get(id=id)
  except (ResearchLab.DoesNotExist, ValueError):
    return Response(f'Deletion failed, Lab with ID {id} does not exist.', status=status.HTTP_404_NOT_FOUND)

  lab.delete()
  return Response(status=status.HTTP_200_OK)

@transaction.atomic
@api_view(['PUT'])
def delete_project_from_profile(request):
  profile_id = request.data.get('profileID')
  project_id = request.data.get('itemID')

  try:
    profile = CompetenceProfile.objects.get(id=profile_id)
  except (CompetenceProfile.DoesNotExist, ValueError):
    return Response(f'Deletion failed, Profile with ID {profile_id} does not exist.', status=status.HTTP_404_NOT_FOUND)

  try:
    project = ResearchProject.objects.get(id=project_id)
  except (ResearchProject.DoesNotExist, ValueError):
    return Response(f'Deletion failed, Project with ID {project_id} does not exist.', status=status.HTTP_404_NOT_FOUND)

  profile.research_projects.remove(project)
  return Response(status=status.HTTP_200_OK)

@transaction.atomic
@api_view(['PUT'])
def delete_lab_from_profile(request):
  profile_id = request.data.get('profileID')
  lab_id = request.data.get('itemID')

  try:
    profile = CompetenceProfile.objects.get(id=profile_id)
  except (CompetenceProfile.DoesNotExist, ValueError):
    return Response(f'Deletion failed, Profile with ID {profile_id} does not exist.', status=status.HTTP_404_NOT_FOUND)

  try:
    lab = ResearchLab.objects.get(id=lab_id)
  except (ResearchLab.DoesNotExist, ValueError):
    return Response(f'Deletion failed, Lab with ID {lab_id} does not exist.', status=status.HTTP_404_NOT_FOUND)

  profile.research_labs.remove(lab)
  return Response(status=status.HTTP_200_OK)

# currently not in use
# todo: give people list of existing users at profile creation
# then at profile deletion scrape the db to see if user is still associated with other profiles
# delete if not 
@transaction.atomic
@api_view(['DELETE'])
def delete_user(request, id):
  try:
    user = User.objects.get(id=id)
  except (User.DoesNotExist, ValueError):
    return Response(f'Deletion failed, User with ID {id} does not exist.', status=status.HTTP_404_NOT_FOUND)

  utils.deleteFile('media/representatives/' + user.photo)
  user.delete()
  return Response(status=status.HTTP_200_OK)
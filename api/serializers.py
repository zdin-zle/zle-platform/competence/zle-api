from rest_framework import serializers
from .models import Institute, User, CompetenceProfile, ResearchNetwork, ResearchNetworkNews, ResearchCluster, ResearchProject, ResearchLab, ResearchNetworkEvent, Address
from api.utils import utils
from django.conf import settings

# serializers for internal use
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'firstname', 'lastname', 'title',
                  'email', 'position', 'photo', 'orc_id']

class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = ['id', 'street', 'city', 'state',
                  'country', 'zip', 'latitude', 'longitude']

    # override representattion method, to figure out if lat and lng are not provided and then search for them again
    def to_representation(self, instance):
        data = super().to_representation(instance)
        if data['latitude'] is None or data['longitude'] is None:
            lat, lng = utils.get_lat_lng(
                data['street'], data['city'], data['zip'], data['country'])
            instance.latitude = lat
            instance.longitude = lng
            instance.save()
        return data

class NetworkSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResearchNetwork
        fields = ['id', 'shortcut', 'name', 'logo']

class ClusterSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResearchCluster
        fields = ['id', 'name']


# fundamental serializers
class ProjectSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, allow_null=True)

    class Meta:
        model = ResearchProject
        fields = ['id', 'name', 'description']

class LabSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, allow_null=True)

    class Meta:
        model = ResearchLab
        fields = ['id', 'name', 'description']

class NetworkNewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResearchNetworkNews
        fields = ['id', 'title', 'photo', 'created',
                  'updated', 'description', 'eyecatcher']

class NetworkEventSerializer(serializers.ModelSerializer):
    address = AddressSerializer(read_only=True)

    class Meta:
        model = ResearchNetworkEvent
        fields = ['id', 'name', 'description', 'photo', 'date', 'address']


# serializers for list viewsod
class ProfileListSerializer(serializers.ModelSerializer):
    members = serializers.PrimaryKeyRelatedField(
        source='competenceprofile_set', many=True, read_only=True
    )
    class Meta:
        model = CompetenceProfile
        fields = ['id', 'shortcut', 'title', 'description', 'logo', 'members']

class NetworkListSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResearchNetwork
        fields = ['id', 'shortcut', 'name', 'logo',]

class ClusterListSerializer(serializers.ModelSerializer):
    members = ProfileListSerializer(
        source='competenceprofile_set', many=True, read_only=True)

    class Meta:
        model = ResearchCluster
        fields = ['id', 'name', 'description', 'members']


# serializers for map views
class ProfileMapSerializer(serializers.ModelSerializer):
    address = AddressSerializer(read_only=True)

    class Meta:
        model = CompetenceProfile
        fields = ['id', 'shortcut', 'title', 'address', 'logo']

class NetworkMapSerializer(serializers.ModelSerializer):
    address = AddressSerializer(read_only=True)
    # members = ProfileListSerializer(source='competenceprofile_set', many=True, read_only=True)
    members = serializers.PrimaryKeyRelatedField(
        source='competenceprofile_set', many=True, read_only=True)

    class Meta:
        model = ResearchNetwork
        fields = ['id', 'shortcut', 'name', 'logo', 'address', 'members']

class ClusterMapSerializer(serializers.ModelSerializer):
    # members = ProfileListSerializer(source='competenceprofile_set', many=True, read_only=True)
    members = serializers.PrimaryKeyRelatedField(
        source='competenceprofile_set', many=True, read_only=True)

    class Meta:
        model = ResearchCluster
        fields = ['id', 'name', 'description', 'members']


# serializers for detailed views of the individual objects
class ClusterDetailSerializer(serializers.ModelSerializer):
    members = ProfileListSerializer(
        source='competenceprofile_set', many=True, read_only=True)

    class Meta:
        model = ResearchCluster
        fields = ['id', 'name', 'description', 'members']

class NetworkDetailSerializer(serializers.ModelSerializer):
    news = NetworkNewsSerializer(many=True, read_only=True)
    events = NetworkEventSerializer(many=True, read_only=True)
    members = ProfileListSerializer(
        source='competenceprofile_set', many=True, read_only=True
    )
    address = AddressSerializer(read_only=True)

    class Meta:
        model = ResearchNetwork
        fields = ['id', 'shortcut', 'name', 'logo', 'events', 'news', 'description',
                  'research_description', 'members', 'address', 'external_link']
        
class InstituteSerializer(serializers.ModelSerializer):
    # also used in list views
    members = serializers.PrimaryKeyRelatedField(
        source='competenceprofile_set', many=True, read_only=True
    )
    class Meta:
        model = CompetenceProfile
        fields = ['id', 'title', 'shortcut', 'description', 'external_link', 'logo', 'members']

class ProfileDetailSerializer(serializers.ModelSerializer):
    author            = UserSerializer   (           read_only=True)
    representatives   = UserSerializer   (many=True, read_only=True)
    maintainers       = UserSerializer   (many=True, read_only=True)
    address           = AddressSerializer(           read_only=True)

    institute         = InstituteSerializer(read_only=True)

    # rename the fields for output
    projects = ProjectSerializer(source='research_projects', many=True, read_only=True)
    labs     = LabSerializer    (source='research_labs',     many=True, read_only=True)
    networks = NetworkSerializer(source='research_networks', many=True, read_only=True)
    clusters = ClusterSerializer(source='research_clusters', many=True, read_only=True)

    class Meta:
        model = CompetenceProfile
        fields = ['id', 'created', 'updated', 'title', 'organization', 'institute', 'shortcut', 'description', 'logo', 'clusters', 'publications',
                  'author', 'representatives', 'maintainers', 'projects', 'labs', 'address', 'external_link', 'networks', 'research_focus']

    # to make sure that profiles get associated with the correct networks and clusters
    def to_representation(self, instance):
        data = super().to_representation(instance)

        # Filter the related research_networks and research_clusters based on the specific profile
        data['networks'] = NetworkSerializer(
            instance.research_networks.all(), many=True).data
        data['clusters'] = ClusterSerializer(
            instance.research_clusters.all(), many=True).data
        return data

# QuerySerializer for parameter of search query
class QuerySerializer(serializers.Serializer):
    search = serializers.CharField(required=False, allow_blank=True,
                                   max_length=settings.TEXT_FORM_FIELDS_LENGTH, help_text='search term')


# Input serializers
# needed when creating new objects differ from the detail serializers
class AddressInputSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = ['id', 'street', 'zip', 'city', 'country']

    # Decapitalize and strip leading/trailing whitespaces for all text fields
    def validate(self, data):
        for field in ['street', 'zip', 'city', 'country']:
            if field in data and isinstance(data[field], str):
                data[field] = data[field].strip().lower()
            return data

class InstituteInputSerializer(serializers.ModelSerializer):
    class Meta:
        model = CompetenceProfile
        fields = ['id', 'title', 'shortcut', 'description', 'external_link', 'logo']

class ProfileInputSerializer(serializers.ModelSerializer):
    representatives = UserSerializer(many=True)
    maintainers = UserSerializer(many=True)

    # make sure that institute is a single number relating to the id of the institute object. Wich is a foreign key in the CompetenceProfile model
    institute = serializers.PrimaryKeyRelatedField(queryset=Institute.objects.all(), required=False, allow_null=True)

    class Meta:
        model = CompetenceProfile
        fields = '__all__'

class NetworkInputSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResearchNetwork
        fields = '__all__'

class ClusterInputSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResearchCluster
        fields = '__all__'

class NetworkEventInputSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResearchNetworkEvent
        fields = '__all__'

class NetworkNewsInputSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResearchNetworkNews
        fields = '__all__'

class UploadFileSerializer(serializers.Serializer):
    file = serializers.FileField(
        allow_empty_file=False,
        use_url=False,
        required=True,
        write_only=True,
        help_text='File to be uploaded'
    )

    class Meta:
        fields = ['file']
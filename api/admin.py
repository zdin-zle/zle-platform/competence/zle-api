from django.contrib import admin
from .models import Institute, ResearchCluster, ResearchLab, CompetenceProfile, ResearchNetworkEvent, ResearchNetworkNews, ResearchProject, Address, ResearchNetwork, User
ResearchLab
admin.site.register(ResearchCluster)
admin.site.register(CompetenceProfile)
admin.site.register(ResearchNetworkEvent)
admin.site.register(ResearchNetworkNews)
admin.site.register(ResearchLab)
admin.site.register(ResearchProject)
admin.site.register(Address)
admin.site.register(ResearchNetwork)
admin.site.register(User)
admin.site.register(Institute)

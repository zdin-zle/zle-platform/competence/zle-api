# image with precompiled pandas for data_init script 
# FROM nickgryg/alpine-pandas:3.10

FROM python:3.12.4-alpine
COPY . /code/
WORKDIR /code
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
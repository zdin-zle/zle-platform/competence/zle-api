# ZLE-Competence Element: ZLE-api

The backend for the ZLE-Competence application. In the competence platform a user can create a profile, network and cluster to manage and see comeptence fields in a given research area. With the help of the ZLE Competence application, a user can find others with the same or similar competence fields.

To run it, you need to have the frontend running as well.
The backend is based on Django. For local testing, a lightweight Sqlite3 Database is used and django can simply be started with its `runserver` command. In Production Django is running inside a docker container and the database is a Postgres Database running in a separate Container. The docker setup can be installed locally aswell, but is not recommended for actual development.

>Links
>- Django admin Panel: <br>
http://localhost:8000/competencebackend/admin/ <br>
https://dev-zle.offis.de/competencebackend/admin/ <br>
>- Swagger: <br>
http://localhost:8000/competencebackend/swagger/ <br>
https://dev-zle.offis.de/competencebackend/swagger/ <br>
>- Media files: <br>
https://dev-zle.offis.de/competencebackend/media/ <br>
https://dev-zle.offis.de/competencebackend/static/

<br><br>

# Documentation 

see [API Documentation](https://dev-zle.offis.de/competence/docs/)

### Deployment Details
- `docker ps`
  - lists all running containers
- `docker exec -it <container_name> /bin/sh`
  - opens a shell in the container
- `docker logs <container_name>`
  - shows the logs of the container
  
<br><br>


# Installation
## Django Runserver (development setup)
1. copy .env file into root directory 
>example .env file used for testing:
>```
>COMPETENCE_DB_ADMIN=competence_admin
>COMPETENCE_DB_PASSWORD=11n*!T6pR2@MM6r8&95R
>COMPETENCE_SECRET_KEY=3f%L4dJt75jthUrAdb9L*!rkFD*qsJkTmt8&>9ELJu^Fsf00JP&
>COMPETENCE_SUPERUSER=root
>COMPETENCE_SUPERUSER_PASSWORD=6PJu4TZ2nMWb
>```

2. Create a virtual environment (optional but recommended)
```bash
python3 -m venv venv
source venv/bin/activate  
# On Windows, use 
"venv\Scripts\activate"
```

3. Install dependencies
```bash
pip3 install -r requirements.txt
# maybe some dependencies are missing -> proceed and install them if they pop up as error message
```
4. Setup database
```bash
python3 manage.py makemigrations
python3 manage.py migrate

python3 read_init_data.py
# deletes and initialize database with test data, also resets id counter for postgresql
# ignore "no such function: setval" error -> id counter resetting not relevant for sqlite3
```
5. Run development server
```bash
source venv/bin/activate #if not already activated
python3 -u manage.py runserver 0.0.0.0:8000
```

then:
- proceed with frontend installation
- or visit django admin panel:
http://localhost:8000/competencebackend/admin/ <br> login with superuser credentials (from .env file)


## Local installation via Docker
>for testing purposes of docker functioniality, not recommended for development

1. Install Docker

https://www.docker.com/products/docker-desktop/

2. copy .env file into root directory <br>
example .env file used for testing:
```
COMPETENCE_DB_ADMIN=competence_admin
COMPETENCE_DB_PASSWORD=11n*!T6pR2@MM6r8&95R
COMPETENCE_SECRET_KEY=3f%L4dJt75jthUrAdb9L*!rkFD*qsJkTmt8&9ELJu^Fsf00JP&
COMPETENCE_SUPERUSER=root
COMPETENCE_SUPERUSER_PASSWORD=6PJu4TZ2nMWb
```

3. then build api image and compose
```bash
docker build -t competence_api_image .
docker compose up -d
``` 
4. go into zle-api container using the desktop app and migrate db:
```bash
python3 manage.py makemigrations  #if migrations have not already been run in the code base
python manage.py migrate
python3 read_init_data.py
```
if migration fails
```bash
python manage.py migrate api zero
python manage.py migrate
```
then
- proceed with frontend installation
- or visit django admin panel:
http://localhost:8000/competencebackend/admin/ <br> login with superuser credentials (from .env file)

<br><br>

# Live Deployment 
 
>VM4 is a virtual machine hosted by OFFIS. It runs some other parts of the Platform (auth, methods, search, transparency) as well. <br>
The vm runs nginx which routes the requests to the correct service. <br>
The competence component which is called zle-api is running in a docker container on the vm. <br>
The database for the competence component is a sepereate postgres database running in its own container on the vm. <br>

1. commit and push changes to gitlab
2. ssh into vm
3. get super user acces
```bash
sudo su
```
4. backup your old database ([see backup and restore](#backup-and-restore-postgres-db))
5. clone repo, or pull changes
6. go into zle-api repo folder and build docker image
```bash
docker build -t competence_api_image .
```
7. if changes got made to the docker-compose.prod.yml file, copy it into the repo folder
```bash 
cp docker-compose.prod.yml /home/dev/competence/competence_api
```
8. compose image with
```bash
docker compose -f /home/dev/competence/competence_api/docker-compose.prod.yml  up -d
```
9. ssh into container and migrate data
```bash
docker exec -it competence_api /bin/sh
python3 manage.py makemigrations # should not be necesseray if you made the migrations in the codebase already
python3 manage.py migrate
```
10. copy backend assets into docker media volume (maybe not necessary)

- if you add or change the original assets in the media folder of the backend the changes won't be automatically copied <br>
-> copy them manually with scp into vm4 and then with docker copy in the volume

Troubleshooting:
- try restarting zle-api container if it doesn't connect to postgres
- sometimes if there are problems which wont be solved by rebuilding and restarting the the docker container the last option is to restart nginx with
```bash
systemctl restart nginx
```

<br><br>

## Backup and restore postgres db
### Backup
```bash
cd /home/dev/competence/postgres_backup
docker exec -t competence_postgres_db pg_dumpall -c -U competence_admin > dump_$(date "+%d-%m-%Y"_"%H_%M_%S").sql
```
### Restore
```bash
cd /home/dev/competence/postgres_backup
cat your_dump.sql | docker exec -i competence_postgres_db psql -U competence_admin
```
<br><br>

## Navigation through postgres db

go into container
```bash
docker exec -it competence_postgres_db /bin/sh
```
and use
```bash
psql -U competence_admin -W competence
password is found in "/home/dev/competence/competence_api/.env"

list tables: \dt
list table schema: \d api_researchproject
list table: Select * from api_user;
```